import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Product from '../component/Product';
import { importCart, setStateModal, removeCart } from '../store/actions'
import Modal from '../component/Modal';
import { Formik } from 'formik'
const CartPage = () => {
    const dispatch = useDispatch();
    const productsCart = useSelector(state => state.cart);
    const productKey = useSelector(state => state.isOpenModal.key);
    const toggleModal = () =>{
        dispatch(setStateModal());
    }
    const confirmAction = () => {
        dispatch(setStateModal())
        dispatch(removeCart(productKey))
    }
    useEffect(() => {
        const localCart = localStorage.getItem('cart');
        {localCart && (dispatch(importCart(JSON.parse(localCart))))}
    }, [])
    return (
        <>
            <Modal
                closeModal={toggleModal}
                header="Подтвердите действие"
                text="Вы действительно хотите выполнить это действие?"
                onClick={confirmAction}
            />
            {
                productsCart.map((item, id) => {
                    return (
                        <>
                            <Product
                                fullProduct={item}
                                key={id}
                                name={item.name}
                                price={item.price}
                                image={item.image}
                                article={item.article}
                                text="Удалить из корзины"
                            />
                        </>
                    )
                })
            }
            <div className="form">
                <Formik
                    initialValues={{firstName: '', lastName: '', age: '', address: '', tel: ''}}
                    onSubmit={(values, { setSubmitting }) => {
                        const {firstName, lastName, age, address, tel} = values;
                        setSubmitting(true)
                        setTimeout(() =>{
                            const form = {
                                firstName: firstName,
                                lastName: lastName,
                                age: age,
                                address: address,
                                tel: tel
                            }
                            console.log(form)
                            console.log(productsCart)
                            localStorage.setItem('form', JSON.stringify(form))
                            localStorage.setItem('cart', '')
                            setSubmitting(false)
                        }, 2000)
                    }}
                    validate={values =>{
                        const {firstName, lastName, age, address, tel} = values;
                        const errors = {}
                        if(!firstName) {
                            errors.firstName = 'Empty firstName'
                        }
                        if(!lastName) {
                            errors.lastName = 'Empty lastName'
                        }
                        if(!age) {
                            errors.age = 'Empty age'
                        }
                        if(!address) {
                            errors.address = 'Empty address'
                        }
                        if(!tel) {
                            errors.tel = 'Empty tel'
                        }
                        return errors
                    }}
                >
                    {
                        ({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <input
                                    type="text"
                                    name="firstName"
                                    placeholder="FirstName"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.firstName}
                                />
                                {errors.firstName && touched.firstName && ( <p>{errors.firstName}</p> )}
                                <input
                                    type="text"
                                    name="lastName"
                                    placeholder="LastName"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.lastName}
                                />
                                {errors.lastName && touched.lastName && ( <p>{errors.lastName}</p> )}
                                <input
                                    type="number"
                                    name="age"
                                    placeholder="Age"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.age}
                                />
                                {errors.age && touched.age && ( <p>{errors.age}</p> )}
                                <input
                                    type="text"
                                    name="address"
                                    placeholder="Address"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.address}
                                />
                                {errors.address && touched.address && ( <p>{errors.address}</p> )}
                                <input
                                    type="number"
                                    name="tel"
                                    placeholder="Tel"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.tel}
                                />
                                {errors.tel && touched.tel && ( <p>{errors.tel}</p> )}
                                <button type="submit" disabled={isSubmitting}> Submit </button>
                            </form>
                        )
                    }
                </Formik>
            </div>
        </>
    )
};

export default CartPage